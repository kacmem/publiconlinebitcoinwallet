var express = require('express'),
    hbs = require('hbs'),
    fs = require('fs'),
    config = require('./config.js'),
    router = require('./router.js'),
    bitcoin = require('bitcoin');
    
app = express();

var client = new bitcoin.Client({
    host: 'localhost',
    port: 8332,
    user: 'login',
    pass: 'pass'
});

app.configure(function() {
    app.set('port', config.PORT);
    app.set('addr', config.ADDR);
    
    app.set('view engine', 'hbs');
    app.set('views', __dirname + '/views');
    
    app.use(express.static(__dirname + '/public'));
    app.use(express.bodyParser());
    app.use(express.logger());
    app.use(app.router);
    
    // register partial
    var partialsDir = __dirname + "/partials/";
    hbs.registerPartial('head', fs.readFileSync(partialsDir + "head.hbs", 'utf-8'));
    hbs.registerPartial('footer', fs.readFileSync(partialsDir + "footer.hbs", 'utf-8'));
});

app.listen(app.get('port'), app.get('addr'), function () {
    console.log("Server started at " + app.get("addr") + ":" + app.get("port"));
    router(app, client);
});